#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>


struct crossword {
    char arr[100][100];
    int sz;
};

struct wordHead {
    int x;
    int y;
    int id;
};

struct wordHead *getDown(const struct crossword* c, int *downIndex) {
    struct wordHead *temp = (struct wordHead *) malloc(sizeof (struct wordHead) * c->sz * c->sz);
    for (int i=0; i < c->sz; i++) {
        int countSpace = 0;
        for(int j=0; j < c->sz; j++) {
            if (c->arr[j][i] == ' ' || c->arr[j][i] == '.'){
                countSpace++;
            } else {
                if (countSpace >= 2) {
                    temp[*downIndex].x = i;
                    temp[*downIndex].y = j - countSpace;
                    countSpace = 0;
                    *downIndex += 1;
                } else {
                    countSpace = 0;
                }
            }
        }
        if (countSpace >= 2) {
            temp[*downIndex].x = i;
            temp[*downIndex].y = c->sz - countSpace;
            *downIndex += 1;
        }
    }
    return temp;
}

struct wordHead *getAcross(const struct crossword* c, int *acrossIndex) {
    struct wordHead *temp = (struct wordHead *) malloc(sizeof (struct wordHead) * c->sz * c->sz);
    for (int i=0; i < c->sz; i++) {
        int countSpace = 0;
        for(int j=0; j < c->sz; j++) {
            if (c->arr[i][j] == ' ' || c->arr[i][j] == '.'){
                countSpace++;
            } else {
                if (countSpace >= 2) {
                    temp[*acrossIndex].x = j - countSpace;
                    temp[*acrossIndex].y = i;
                    countSpace = 0;
                    *acrossIndex += 1;
                } else {
                    countSpace = 0;
                }
            }
        }
        if (countSpace >= 2) {
            temp[*acrossIndex].x = c->sz - countSpace;
            temp[*acrossIndex].y = i;
            *acrossIndex += 1;
        }
    }
    return temp;
}

void sortClue(struct wordHead *arr,int size) {
    for (int i=0; i < size; i++) {
        for (int j=0; j < size; j++) {
            if (arr[i].id < arr[j].id) {
                struct wordHead temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;
            }
        }
    }
}

void getcluestring(const struct crossword* c, char *ans) {
    int acrossIndex = 0;
    int downIndex = 0;
    struct wordHead *across = getAcross(c, &acrossIndex);
    struct wordHead *down = getDown(c, &downIndex);

    int order = 1;
    for (int i = 0; i < c->sz; i++) {
        for (int j = 0; j < c->sz; j++) {
            bool check = false;
            for (int z =0; z < acrossIndex; z++) {
                if (across[z].x == j && across[z].y == i) {
                    across[z].id = order;
                    check = true;
                }
            }
            for (int z =0; z < downIndex; z++) {
                if (down[z].x == j && down[z].y == i) {
                    down[z].id = order;
                    check = true;
                }
            }
            if (check) {
                order++;
            }
        }
    }
    sortClue(down, downIndex);
    char *acrossResult = (char *) malloc(sizeof (char) * acrossIndex * 2 + 1);
    sprintf(acrossResult, "A");
    for (int i = 0; i < acrossIndex; i++) {
        char *intToString = (char *) malloc(sizeof (char)*3);
        sprintf(intToString,"-%d", across[i].id);
        strcat(acrossResult, intToString);
    }
    char *downResult = (char *) malloc(sizeof (char) * downIndex * 2 + 1);
    sprintf(downResult, "D");
    for (int i = 0; i < downIndex; i++) {
        char *intToString = (char *) malloc(sizeof (char)*3);
        sprintf(intToString,"-%d", down[i].id);
        strcat(downResult, intToString);
    }
    sprintf(ans, "%s|%s", acrossResult, downResult);
}



bool str2crossword(int sz, char* ip, struct crossword* cw) {
    if (sz < 0 || ip == NULL || cw ==NULL) {
        return false;
    }
    cw->sz = sz;
    for (int i=0; i < strlen(ip); i++) {
        cw->arr[i/sz][i%sz] = ip[i];
    }
    for(int y=0; y < sz; y++){
        for(int x=0; x < sz; x++){
            printf("%c", cw->arr[y][x]);
        }
        printf("\n");
    }
    return true;
}


int getchecked(struct crossword c) {
    int orangeBlock[c.sz][c.sz];
    for (int i=0; i < c.sz; i++) {
        for (int j=0; j< c.sz; j++) {
            orangeBlock[i][j] = 0;
        }
    }

    for (int i=0; i < c.sz; i++) {
        int countSpace = 0;
        for(int j=0; j < c.sz; j++) {
            if (c.arr[j][i] == ' ' || c.arr[j][i] == '.'){
                countSpace++;
            } else {
                if (countSpace >= 2) {
                    for(int s = 0; s < countSpace; s++) {
                        orangeBlock[j-s -1][i]+=1;
                    }
                    countSpace = 0;
                } else {
                    countSpace = 0;
                }
            }
        }
        if (countSpace >= 2) {
            for(int s = 0; s < countSpace; s++) {
                orangeBlock[c.sz-s - 1][i]+=1;
            }
        }
    }

    for (int i=0; i < c.sz; i++) {
        int countSpace = 0;
        for(int j=0; j < c.sz; j++) {
            if (c.arr[i][j] == ' ' || c.arr[i][j] == '.'){
                countSpace++;
            } else {
                if (countSpace >= 2) {
                    for(int s = 0; s < countSpace; s++) {
                        orangeBlock[i][j-s-1]+=1;
                    }
                    countSpace = 0;
                } else {
                    countSpace = 0;
                }
            }
        }
        if (countSpace >= 2) {
            for(int s = 0; s < countSpace; s++) {
                orangeBlock[i][c.sz- s-1]+=1;
            }
        }
    }
    float countOne = 0;
    float countTwo = 0;
    for (int i=0; i < c.sz; i++) {
        for (int j=0; j< c.sz; j++) {
            if (orangeBlock[i][j] ==1) {
                countOne++;
            }
            if (orangeBlock[i][j] ==2) {
                countTwo++;
            }
        }
    }
    float cal = (countTwo * 100 / (countOne + countTwo));
    int ans = (int)(cal + 0.5);
    printf("%d", ans);
    return ans;
}


int main() {
    char *l = ".....X.XX.X.X..........XX.X.X......X.X.XX..........X.X.XX.X.....";
    char ans[100];
    int sz = 8;
    struct crossword *cw;
    cw = (struct  crossword *) malloc(sizeof(struct crossword));
    str2crossword(sz, l, cw);
    getcluestring(cw, ans);
    printf("%s\n", ans);
    printf("%d\n", strcmp("A-1-5-6-7-8-11-12-13|D-2-3-4-5-9-10", ans));
    getchecked(*cw);


    return 0;
}
