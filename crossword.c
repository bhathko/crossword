#include "crossword.h"

struct wordHead {
    int x;
    int y;
    int id;
};

// Might be useful to be able to print them
// to hep with debugging
void print_crossword(const crossword* c)
{
   for(int y=0; y<c->sz; y++){
      for(int x=0; x<c->sz; x++){
         printf("%c", c->arr[y][x]);
      }
      printf("\n");
   }
}

bool str2crossword(int sz, char* ip, crossword* cw) {
    unsigned long ipLength = sz * sz;
    if (sz < 0 || ip == NULL || cw ==NULL || strlen(ip) != ipLength) {
        return false;
    }
    cw->sz = sz;
    for (unsigned long i=0; i < strlen(ip); i++) {
        cw->arr[i/sz][i%sz] = ip[i];
    }
    return true;
}


struct wordHead *getDown(const struct crossword* c, int *downIndex) {
    struct wordHead *temp = (struct wordHead *) malloc(sizeof (struct wordHead) * c->sz * c->sz);
    for (int i=0; i < c->sz; i++) {
        int countSpace = 0;
        for(int j=0; j < c->sz; j++) {
            if (c->arr[j][i] == ' ' || c->arr[j][i] == '.'){
                countSpace++;
            } else {
                if (countSpace >= 2) {
                    temp[*downIndex].x = i;
                    temp[*downIndex].y = j - countSpace;
                    countSpace = 0;
                    *downIndex += 1;
                } else {
                    countSpace = 0;
                }
            }
        }
        if (countSpace >= 2) {
            temp[*downIndex].x = i;
            temp[*downIndex].y = c->sz - countSpace;
            *downIndex += 1;
        }
    }
    return temp;
}

struct wordHead *getAcross(const struct crossword* c, int *acrossIndex) {
    struct wordHead *temp = (struct wordHead *) malloc(sizeof (struct wordHead) * c->sz * c->sz);
    for (int i=0; i < c->sz; i++) {
        int countSpace = 0;
        for(int j=0; j < c->sz; j++) {
            if (c->arr[i][j] == ' ' || c->arr[i][j] == '.'){
                countSpace++;
            } else {
                if (countSpace >= 2) {
                    temp[*acrossIndex].x = j - countSpace;
                    temp[*acrossIndex].y = i;
                    countSpace = 0;
                    *acrossIndex += 1;
                } else {
                    countSpace = 0;
                }
            }
        }
        if (countSpace >= 2) {
            temp[*acrossIndex].x = c->sz - countSpace;
            temp[*acrossIndex].y = i;
            *acrossIndex += 1;
        }
    }
    return temp;
}

void sortClue(struct wordHead *arr,int size) {
    for (int i=0; i < size; i++) {
        for (int j=0; j < size; j++) {
            if (arr[i].id < arr[j].id) {
                struct wordHead temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;
            }
        }
    }
}


int getchecked(crossword c) {
    int orangeBlock[100][100];
    for (int i=0; i < 100; i++) {
        for (int j=0; j< 100; j++) {
            orangeBlock[i][j] = 0;
        }
    }

    for (int i=0; i < c.sz; i++) {
        int countSpace = 0;
        for(int j=0; j < c.sz; j++) {
            if (c.arr[j][i] == ' ' || c.arr[j][i] == '.'){
                countSpace++;
            } else {
                if (countSpace >= 2) {
                    for(int s = 0; s < countSpace; s++) {
                        orangeBlock[j-s -1][i]+=1;
                    }
                    countSpace = 0;
                } else {
                    countSpace = 0;
                }
            }
        }
        if (countSpace >= 2) {
            for(int s = 0; s < countSpace; s++) {
                orangeBlock[c.sz-s - 1][i]+=1;
            }
        }
    }

    for (int i=0; i < c.sz; i++) {
        int countSpace = 0;
        for(int j=0; j < c.sz; j++) {
            if (c.arr[i][j] == ' ' || c.arr[i][j] == '.'){
                countSpace++;
            } else {
                if (countSpace >= 2) {
                    for(int s = 0; s < countSpace; s++) {
                        orangeBlock[i][j-s-1]+=1;
                    }
                    countSpace = 0;
                } else {
                    countSpace = 0;
                }
            }
        }
        if (countSpace >= 2) {
            for(int s = 0; s < countSpace; s++) {
                orangeBlock[i][c.sz- s-1]+=1;
            }
        }
    }
    float countOne = 0;
    float countTwo = 0;
    for (int i=0; i < c.sz; i++) {
        for (int j=0; j< c.sz; j++) {
            if (orangeBlock[i][j] ==1) {
                countOne++;
            }
            if (orangeBlock[i][j] ==2) {
                countTwo++;
            }
        }
    }

    float cal = (countTwo * 100 / (countOne + countTwo));
    int ans = (int)(cal + 0.5);
    return ans;
}

void getcluestring(const crossword* c, char* ans) {
    int acrossIndex = 0;
    int downIndex = 0;
    struct wordHead *across = getAcross(c, &acrossIndex);
    struct wordHead *down = getDown(c, &downIndex);

    int order = 1;
    for (int i = 0; i < c->sz; i++) {
        for (int j = 0; j < c->sz; j++) {
            bool check = false;
            for (int z =0; z < acrossIndex; z++) {
                if (across[z].x == j && across[z].y == i) {
                    across[z].id = order;
                    check = true;
                }
            }
            for (int z =0; z < downIndex; z++) {
                if (down[z].x == j && down[z].y == i) {
                    down[z].id = order;
                    check = true;
                }
            }
            if (check) {
                order++;
            }
        }
    }
    sortClue(down, downIndex);
    char *acrossResult = (char *) malloc(sizeof (char) * acrossIndex * 2 + 1);
    sprintf(acrossResult, "A");
    for (int i = 0; i < acrossIndex; i++) {
        char *intToString = (char *) malloc(sizeof (char)*3);
        sprintf(intToString,"-%d", across[i].id);
        strcat(acrossResult, intToString);
    }
    char *downResult = (char *) malloc(sizeof (char) * downIndex * 2 + 1);
    sprintf(downResult, "D");
    for (int i = 0; i < downIndex; i++) {
        char *intToString = (char *) malloc(sizeof (char)*3);
        sprintf(intToString,"-%d", down[i].id);
        strcat(downResult, intToString);
    }
    sprintf(ans, "%s|%s", acrossResult, downResult);
    free(across);
    free(down);
    free(acrossResult);
    free(downResult);
}

void test(void) {

}
